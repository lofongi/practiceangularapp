import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Athlete } from 'src/app/models/athlete';

@Injectable({
  providedIn: 'root'
})
export class AthleteService {
  constructor(private http: HttpClient){

  }

  configUrl = 'https://my-json-server.typicode.com/Andriy-Yahello/athletes/athletes';
  athletes: Array<Athlete>;

  getAthletes() {

  return this.http.get<Athlete[]>(
    this.configUrl);

  // getAthletes(){
  //   return [
  //   {name:"Jan Frodeno", country: "DEU", time: "08:06:30"},
  //   {name:"Sebastian Kienle", country: "DEU", time: "08:10:02"},
  //   {name:"Patrick Lange", country: "DEU", time: "08:11:14"},
  //   {name:"Ben Hoffman", country: "USA", time: "08:13:00"},
  //   {name:"Andi Boecherer", country: "DEU", time: "08:13:25"}
  // ];
  }
}
