import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AthleteListComponent } from './athlete-list/athlete-list.component';
import { AthleteComponent } from './athlete-list/athlete/athlete.component';

@NgModule({
  declarations: [
    AppComponent,
    AthleteListComponent,
    AthleteComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
