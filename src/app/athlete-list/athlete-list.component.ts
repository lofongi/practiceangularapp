import { Component, Output, EventEmitter} from '@angular/core';
import { AthleteService } from 'src/services/athlete.service';
import { Athlete} from "../models/athlete";

@Component({
  selector: 'app-athlete-list',
  templateUrl: './athlete-list.component.html',
  styleUrls: ['./athlete-list.component.css']
})
export class AthleteListComponent {

  athletes: Array<Athlete>;
  @Output() selected: EventEmitter<Athlete>= new EventEmitter<Athlete>();

  constructor(private athleteService: AthleteService) { }

  select(selectedAthlete: Athlete){
    this.selected.emit(selectedAthlete);
  }

  getAthletes(){
    this.athleteService.getAthletes()
    .subscribe((resp: Athlete[]) => {
      this.athletes = resp
    });
  }

  ngOnInit(){
    this.getAthletes();
  }
}
