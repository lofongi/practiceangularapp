import { Component, Input } from '@angular/core';
import { Athlete } from 'src/app/models/athlete';

@Component({
  selector: 'app-athlete',
  templateUrl: './athlete.component.html',
  styleUrls: ['./athlete.component.css']
})
export class AthleteComponent{

  @Input() athlete: Athlete;
  
  constructor() { }
}
