import { Component } from '@angular/core';
import { Athlete } from './models/athlete';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent {
  selectedAthlete: string;
  athletes: Array<any>;
  title = 'AngularApp';
  color = "";

  constructor(){
    this.selectedAthlete = "none";
  }

  setColor(){
    if (this.color == "green")
      this.color = "red";
    else
      this.color = "green";
  }

  showDetails(selectedAthlete: Athlete){
    this.selectedAthlete = selectedAthlete.name;
  }
}
